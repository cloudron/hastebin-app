#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    url = require('url'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    const username = process.env.USERNAME;
    const password = process.env.PASSWORD;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app, pasteUrl;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function addPaste() {
        await browser.get('https://' + app.fqdn);
        await browser.findElement(By.xpath('//textarea')).sendKeys('cloudronapptest');
        await browser.findElement(By.xpath('//button[contains(@class, "save")]')).click();
        await browser.wait(async function () {
            const currentUrl = await browser.getCurrentUrl();
            pasteUrl = currentUrl;
            return url.parse(currentUrl).pathname !== '/';
        });
        console.log('paste url is ', pasteUrl);
    }

    async function login(alreadyAuthenticated) {
        await browser.get(`https://${app.fqdn}/login`);

        await browser.wait(until.elementLocated(By.id('loginProceedButton')));
        await browser.findElement(By.id('loginProceedButton')).click();
        if (!alreadyAuthenticated) {
            await browser.wait(until.elementLocated(By.xpath('//input[@name="username"]')), 5000);
            await browser.findElement(By.xpath('//input[@name="username"]')).sendKeys(username);
            await browser.findElement(By.xpath('//input[@name="password"]')).sendKeys(password);
            await browser.sleep(2000);
            await browser.findElement(By.id('loginSubmitButton')).click();
        }
        await browser.wait(until.elementLocated(By.xpath('//textarea')), TIMEOUT);
        await browser.sleep(2000);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/logout`);
        await browser.wait(until.elementLocated(By.id('loginProceedButton')));
    }

    async function checkPaste() {
        await browser.get(pasteUrl);
        await browser.wait(until.elementLocated(By.xpath('//*[text()="cloudronapptest"]')), TIMEOUT);
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can get the main page', async function () {
        const response = await fetch('https://' + app.fqdn);
        expect(response.status).to.eql(200);
    });

    it('can add a paste', addPaste);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --no-sso --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can see old paste', checkPaste);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    it('install app (sso)', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);

    it('can login', login.bind(null, false));
    it('can add a paste', addPaste);
    it('can see old paste', checkPaste);
    it('can logout', logout);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --no-sso --appstore-id com.hastebin.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });
    it('can add a paste', addPaste);
    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
    });
    it('check paste', checkPaste);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
