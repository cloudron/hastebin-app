FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=git-refs packageName=https://github.com/seejohnrun/haste-server branch=master
ARG HASTEBIN_COMMIT=a56763258b9a4a8fd589484351f027599a4f3515

RUN curl -L https://github.com/seejohnrun/haste-server/tarball/${HASTEBIN_COMMIT} | tar -xz --strip-components 1 -f -

RUN npm install --omit=dev && npm cache clean --force

COPY start.sh config.js.template /app/pkg/

CMD /app/pkg/start.sh
