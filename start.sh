#!/bin/bash

set -eu

if [[ ! -f /app/data/config.json ]]; then
    echo "==> Creating config.json on first run"
    cp /app/pkg/config.js.template /app/data/config.json
fi

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data

echo "==> Starting haste"
exec /usr/local/bin/gosu cloudron:cloudron node /app/code/server.js /app/data/config.json

