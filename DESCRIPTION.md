## About

Haste is a pastebin software.

Major design objectives:

    Be really pretty
    Be really simple

Haste works really well with a little utility called haste-client, allowing you to do things like:

    cat something | haste

which will output a URL to share containing the contents of cat something's STDOUT.

